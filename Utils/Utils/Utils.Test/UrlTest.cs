using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Specialized;

namespace Utils.Test
{
    [TestClass]
    public class UrlTest
    {
        [TestMethod]
        public void ParseQueryStringTest()
        {
            var ps = Utils.Url.ParseQueryString("?username=admin&password=test");
            Assert.AreEqual(ps.Count, 2);

            ps = Utils.Url.ParseQueryString("www.baidu.com?username=admin&password=test");
            Assert.AreEqual(ps.Count, 2);

            ps = Utils.Url.ParseQueryString("username=admin&password=test");
            Assert.AreEqual(ps.Count, 2);

            ps = Utils.Url.ParseQueryString("password=test");
            Assert.AreEqual(ps.Count, 1);
        }

        [TestMethod]
        public void BuildRequestUrlStringTest()
        {
            var queryStr = Utils.Url.BuildRequestUrl("www.baidu.com?test=2", "username,password", "admin", "test");
            Assert.AreEqual(queryStr, "www.baidu.com?test=2&username=admin&password=test");

            queryStr = Utils.Url.BuildRequestUrl("www.baidu.com?test=2", "username,password,test", "admin", "test","");
            Assert.AreEqual(queryStr, "www.baidu.com?test=&username=admin&password=test");

            queryStr = Utils.Url.BuildRequestUrl("www.baidu.com?username=admin", "password,test", "test", "");
            Assert.AreEqual(queryStr, "www.baidu.com?username=admin&password=test&test=");
        }

    }
}
