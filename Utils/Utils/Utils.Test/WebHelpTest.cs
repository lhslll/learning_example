﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Utils.Test
{
    [TestClass]
    public class WebHelpTest
    {
        [TestMethod]
        public void CreateWebRequestTest()
        {
            var request = WebHelp.CreateDefault("http://www.baidu.com");
            Assert.AreEqual(request.GetType(), typeof(HttpWebRequest));
        }

        [TestMethod]
        public void RequestBaiduTest()
        {
            var request = WebHelp.CreateDefault("http://www.baidu.com");
            var rsp=request.GetResponseIgnoreServerError();
            Assert.AreEqual(rsp.StatusCode, HttpStatusCode.OK);
            var rspStr=rsp.GetResponseString();
            Assert.IsNotNull(rspStr);

            request = WebHelp.CreateDefault("http://www.baidu.com/s?w=.net");
            rsp = request.GetResponseIgnoreServerError();
            Assert.AreEqual(rsp.StatusCode, HttpStatusCode.OK);
            rspStr = rsp.GetResponseString();
            Assert.IsNotNull(rspStr);
        }


        [TestMethod]
        public void RequestDownlandFileTest()
        {

            var request = WebHelp.CreateDefault("https://pm.myapp.com/invc/xfspeed/qqpcmgr/download/QQPCDownload110023.exe");
            var rsp = request.GetResponseIgnoreServerError();
            Assert.AreEqual(rsp.StatusCode, HttpStatusCode.OK);
            var datas = rsp.GetResponseFile();
            Assert.IsTrue(datas != null && datas.Length > 0);

            request = WebHelp.CreateDefault("https://pm.myapp.com/invc/xfspeed/qqpcmgr/download/QQPCDownload110023.exe");
            rsp = request.GetResponseIgnoreServerError();
            Assert.AreEqual(rsp.StatusCode, HttpStatusCode.OK);
            var file = rsp.GetResponseFileItem();
            Assert.IsTrue(file != null && file.FileName == "QQPCDownload110023.exe");
        }
    }
}
