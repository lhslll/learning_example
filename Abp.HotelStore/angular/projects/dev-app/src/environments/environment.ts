import { Config } from '@abp/ng.core';

export const environment = {
  production: false,
  application: {
    baseUrl: 'http://localhost:4200/',
    name: 'HotelStore',
    logoUrl: '',
  },
  oAuthConfig: {
    issuer: 'https://localhost:44367',
    clientId: 'HotelStore_ConsoleTestApp',
    dummyClientSecret: '1q2w3e*',
    scope: 'HotelStore',
    oidc: false,
    requireHttps: true,
  },
  apis: {
    default: {
      url: 'https://localhost:44367',
      rootNamespace: 'Acme.HotelStore',
    },
    HotelStore: {
      url: 'https://localhost:44325',
      rootNamespace: 'Acme.HotelStore',
    },
  },
} as Config.Environment;
