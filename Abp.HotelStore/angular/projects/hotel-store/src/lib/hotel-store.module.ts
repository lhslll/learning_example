import { NgModule, NgModuleFactory, ModuleWithProviders } from '@angular/core';
import { CoreModule, LazyModuleFactory } from '@abp/ng.core';
import { ThemeSharedModule } from '@abp/ng.theme.shared';
import { HotelStoreComponent } from './components/hotel-store.component';
import { HotelStoreRoutingModule } from './hotel-store-routing.module';

@NgModule({
  declarations: [HotelStoreComponent],
  imports: [CoreModule, ThemeSharedModule, HotelStoreRoutingModule],
  exports: [HotelStoreComponent],
})
export class HotelStoreModule {
  static forChild(): ModuleWithProviders<HotelStoreModule> {
    return {
      ngModule: HotelStoreModule,
      providers: [],
    };
  }

  static forLazy(): NgModuleFactory<HotelStoreModule> {
    return new LazyModuleFactory(HotelStoreModule.forChild());
  }
}
