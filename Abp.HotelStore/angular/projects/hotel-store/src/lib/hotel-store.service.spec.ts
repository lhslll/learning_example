import { TestBed } from '@angular/core/testing';

import { HotelStoreService } from './hotel-store.service';

describe('HotelStoreService', () => {
  let service: HotelStoreService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HotelStoreService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
