﻿using Volo.Abp.Data;
using Volo.Abp.MongoDB;

namespace Acme.HotelStore.MongoDB
{
    [ConnectionStringName(HotelStoreDbProperties.ConnectionStringName)]
    public interface IHotelStoreMongoDbContext : IAbpMongoDbContext
    {
        /* Define mongo collections here. Example:
         * IMongoCollection<Question> Questions { get; }
         */
    }
}
