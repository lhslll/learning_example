﻿using System;
using System.Collections.Generic;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace Acme.HotelStore.Hotels
{
    public class HotelDto:AuditedEntityDto<Guid>
    {
        public string HotelName { get; set; }

        public string Address { get; set; }
    }
}
