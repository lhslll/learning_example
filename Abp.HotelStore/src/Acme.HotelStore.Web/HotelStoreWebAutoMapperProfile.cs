﻿using AutoMapper;

namespace Acme.HotelStore.Web
{
    public class HotelStoreWebAutoMapperProfile : Profile
    {
        public HotelStoreWebAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
        }
    }
}