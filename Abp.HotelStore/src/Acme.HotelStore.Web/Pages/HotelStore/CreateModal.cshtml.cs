using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Acme.HotelStore.Hotels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Acme.HotelStore.Web.Pages.HotelStore
{
    [Authorize(Permissions.HotelStorePermissions.Hotel.Delete)]
    public class CreateModalModel : HotelStorePageModel
    {
        [BindProperty]
        public CreateOrUpdateHotelDto Hotel { get; set; }

        IHotelAppService hotelAppService;

        public CreateModalModel(IHotelAppService hotelAppService)
        {
            this.hotelAppService = hotelAppService;
        }

        public void OnGet()
        {
            Hotel = new CreateOrUpdateHotelDto();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            await hotelAppService.CreateAsync(Hotel);
            return NoContent();
        }
    }
}
