﻿using Acme.HotelStore.Hotels;
using Microsoft.EntityFrameworkCore;
using Volo.Abp.Data;
using Volo.Abp.EntityFrameworkCore;

namespace Acme.HotelStore.EntityFrameworkCore
{
    [ConnectionStringName(HotelStoreDbProperties.ConnectionStringName)]
    public interface IHotelStoreDbContext : IEfCoreDbContext
    {
        /* Add DbSet for each Aggregate Root here. Example:
         * DbSet<Question> Questions { get; }
         */

        DbSet<Hotel> Hotels { get; }
    }
}