﻿using System;
using Acme.HotelStore.Hotels;
using Microsoft.EntityFrameworkCore;
using Volo.Abp;
using Volo.Abp.EntityFrameworkCore.Modeling;

namespace Acme.HotelStore.EntityFrameworkCore
{
    public static class HotelStoreDbContextModelCreatingExtensions
    {
        public static void ConfigureHotelStore(
            this ModelBuilder builder,
            Action<HotelStoreModelBuilderConfigurationOptions> optionsAction = null)
        {
            Check.NotNull(builder, nameof(builder));

            var options = new HotelStoreModelBuilderConfigurationOptions(
                HotelStoreDbProperties.DbTablePrefix,
                HotelStoreDbProperties.DbSchema
            );

            optionsAction?.Invoke(options);

            builder.Entity<Hotel>(b =>
            {
                b.ToTable(options.TablePrefix + "Hotel", options.Schema);
                b.ConfigureByConvention();
            });

            /* Configure all entities here. Example:

            builder.Entity<Question>(b =>
            {
                //Configure table & schema name
                b.ToTable(options.TablePrefix + "Questions", options.Schema);
            
                b.ConfigureByConvention();
            
                //Properties
                b.Property(q => q.Title).IsRequired().HasMaxLength(QuestionConsts.MaxTitleLength);
                
                //Relations
                b.HasMany(question => question.Tags).WithOne().HasForeignKey(qt => qt.QuestionId);

                //Indexes
                b.HasIndex(q => q.CreationTime);
            });
            */
        }
    }
}