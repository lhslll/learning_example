﻿using Acme.HotelStore.Hotels;
using Acme.HotelStore.Permissions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;

namespace Acme.HotelStore.Hotels
{
    public class HotelAppService: CrudAppService<Hotel, HotelDto, Guid, PagedAndSortedResultRequestDto, CreateOrUpdateHotelDto>,IHotelAppService
    {
        public HotelAppService(IRepository<Hotel, Guid> repository) : base(repository)
        {
            GetPolicyName = HotelStorePermissions.Hotel.Default;
            GetListPolicyName = HotelStorePermissions.Hotel.Default;
            CreatePolicyName = HotelStorePermissions.Hotel.Create;
            UpdatePolicyName = HotelStorePermissions.Hotel.Edit;
            DeletePolicyName = HotelStorePermissions.Hotel.Delete;
        }
        public override async Task<PagedResultDto<HotelDto>> GetListAsync(PagedAndSortedResultRequestDto input)
        {
            return await base.GetListAsync(input);
        }
    }
}
