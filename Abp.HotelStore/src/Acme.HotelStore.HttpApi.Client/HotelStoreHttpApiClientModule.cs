﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.Http.Client;
using Volo.Abp.Modularity;

namespace Acme.HotelStore
{
    [DependsOn(
        typeof(HotelStoreApplicationContractsModule),
        typeof(AbpHttpClientModule))]
    public class HotelStoreHttpApiClientModule : AbpModule
    {
        public const string RemoteServiceName = "HotelStore";

        public override void ConfigureServices(ServiceConfigurationContext context)
        {
            context.Services.AddHttpClientProxies(
                typeof(HotelStoreApplicationContractsModule).Assembly,
                RemoteServiceName
            );
        }
    }
}
