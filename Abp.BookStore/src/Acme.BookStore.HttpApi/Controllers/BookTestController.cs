﻿using Acme.BookStore.Books;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp;
using Volo.Abp.Application.Dtos;

namespace Acme.BookStore.Controllers
{
    public class BookTestController : BookStoreController, IBookAppService
    {
        IBookAppService bookAppService;
        public BookTestController(IBookAppService bookAppService)
        {
            this.bookAppService = bookAppService;
        }
        public async Task<BookDto> CreateAsync(CreateUpdateBookDto input)
        {
            return await bookAppService.CreateAsync(input);
        }

        public async Task DeleteAsync(Guid id)
        {
            await bookAppService.DeleteAsync(id);
        }

        public async Task<BookDto> GetAsync(Guid id)
        {
            return await bookAppService.GetAsync(id);
        }

        public async Task<ListResultDto<AuthorLookupDto>> GetAuthorLookupAsync()
        {
            return await bookAppService.GetAuthorLookupAsync();
        }

        public async Task<PagedResultDto<BookDto>> GetListAsync(PagedAndSortedResultRequestDto input)
        {
            return await bookAppService.GetListAsync(input);
        }

        public async Task<BookDto> UpdateAsync(Guid id, CreateUpdateBookDto input)
        {
            return await bookAppService.UpdateAsync(id, input);
        }
    }
}
