﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.DependencyInjection;
using Volo.Abp.EventBus;
using System.Linq;
using Volo.Abp.Emailing;
using Volo.Abp.EventBus.Distributed;

namespace Acme.BookStore.Authors
{
    public class AuthorEventHandles : IDistributedEventHandler<AuthorNameChangeEvent>, ITransientDependency
    {
        IEmailSender emailSender;
        public AuthorEventHandles(IEmailSender emailSender)
        {
            this.emailSender = emailSender;
        }
        public IAuthorRepository authorRepository { get; set; }
        public async Task HandleEventAsync(AuthorNameChangeEvent eventData)
        {
            var dataBaseAuthor = await authorRepository.GetAsync(eventData.Author.Id);
            await emailSender.SendAsync("sun_yuliang@foxmail.com", "名称变更", $"{eventData.OldName}变为：{eventData.Author.Name}");
        }
    }
}
