﻿using System;
using System.Threading.Tasks;
using Acme.BookStore.Authors;
using Acme.BookStore.Books;
using Volo.Abp.Data;
using Volo.Abp.DependencyInjection;
using Volo.Abp.Domain.Repositories;
using System.Linq;

namespace Acme.BookStore
{
    public class BookStoreDataSeederContributor : IDataSeedContributor, ITransientDependency
    {
        private readonly IRepository<Book, Guid> bookRepository;
        private readonly IAuthorRepository authorRepository;
        private readonly AuthorManager authorManager;

        public BookStoreDataSeederContributor(IRepository<Book, Guid> bookRepository, IAuthorRepository authorRepository, AuthorManager authorManager)
        {
            this.bookRepository = bookRepository;
            this.authorRepository = authorRepository;
            this.authorManager = authorManager;
        }
        public async Task SeedAsync(DataSeedContext context)
        {
            if (await authorRepository.GetCountAsync() == 0)
            {
                await authorRepository.InsertAsync(
                    await authorManager.CreateAsync(
                        "George Orwell",
                        new DateTime(1903, 06, 25),
                        "Orwell produced literary criticism and poetry, fiction and polemical journalism; and is best known for the allegorical novella Animal Farm (1945) and the dystopian novel Nineteen Eighty-Four (1949)."
                    )
                , true);
                await authorRepository.InsertAsync(
                    await authorManager.CreateAsync(
                        "test",
                        new DateTime(1903, 06, 25),
                        "Orwell produced literary criticism and poetry, fiction and polemical journalism; and is best known for the allegorical novella Animal Farm (1945) and the dystopian novel Nineteen Eighty-Four (1949)."
                    )
                , true);
            }

            var author = await authorRepository.FindByNameAsync("test");
            if (author != null) { }
                if (await bookRepository.GetCountAsync() == 0)
                {
                    await bookRepository.InsertAsync(
                        new Book
                        {
                            Name = "1984",
                            Type = BookType.Dystopia,
                            PublishDate = new DateTime(1949, 6, 8),
                            Price = 19.84f,
                            AuthorId = author.Id
                        },
                        autoSave: true
                    );

                    await bookRepository.InsertAsync(
                        new Book
                        {
                            Name = "The Hitchhiker's Guide to the Galaxy",
                            Type = BookType.ScienceFiction,
                            PublishDate = new DateTime(1995, 9, 27),
                            Price = 42.0f,
                            AuthorId = author.Id
                        },
                        autoSave: true
                    );
                }
        }
    }
}
