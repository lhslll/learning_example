﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Acme.BookStore.Books;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Volo.Abp.AspNetCore.Mvc.UI.Bootstrap.TagHelpers.Form;
using Volo.Abp.Features;

namespace Acme.BookStore.Web.Pages.Books
{
    [RequiresFeature("BookStore.Books")]
    public class EditModalModel : BookStorePageModel
    {

        [BindProperty]
        public EditBookViewModel Book { get; set; }

        public List<SelectListItem> Authors { get; set; }

        private readonly IBookAppService _bookAppService;

        public EditModalModel(IBookAppService bookAppService)
        {
            _bookAppService = bookAppService;
        }

        public async Task OnGetAsync(Guid Id)
        {
            Authors = (await _bookAppService.GetAuthorLookupAsync())
                .Items.Select(r => new SelectListItem(r.Name, r.Id.ToString()))
                .ToList();
            var bookDto = await _bookAppService.GetAsync(Id);
            Book = ObjectMapper.Map<BookDto, EditBookViewModel>(bookDto);
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var updateBookDto = ObjectMapper.Map<EditBookViewModel, CreateUpdateBookDto>(Book);
            await _bookAppService.UpdateAsync(Book.Id, updateBookDto);
            return NoContent();
        }

        public class EditBookViewModel
        {
            [HiddenInput]
            [BindProperty(SupportsGet = true)]
            public Guid Id { get; set; }

            [Required]
            [StringLength(BookConsts.MaxNameLength)]
            public string Name { get; set; }

            [Required]
            public BookType Type { get; set; } = BookType.Undefined;

            [Required]
            [DataType(DataType.Date)]
            public DateTime PublishDate { get; set; } = DateTime.Now;

            [Required]
            public float Price { get; set; }

            [SelectItems(nameof(Authors))]
            [DisplayName("Author")]
            [Required]
            public Guid AuthorId { get; set; }
        }
    }
}