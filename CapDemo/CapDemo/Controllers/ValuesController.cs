﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetCore.CAP;
using Microsoft.AspNetCore.Mvc;

namespace CapDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [HttpGet]
        public async Task<string> Get([FromServices]ICapPublisher capPublish)
        {
            await capPublish.PublishAsync<string>("Order.Created", "hello，订单创建成功啦");  //发布Order.Created事件
            return "订单创建成功啦";
        }

        [NonAction]
        [CapSubscribe("Order.Created")]    //监听Order.Created事件
        public async Task OrderCreatedEventHand(string msg)
        {
            Console.WriteLine(msg);
        }
    }
}
