﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EFCore.Models
{
    public class User
    {
        [Key]
        public string UserNmae { get; set; }

        public string RealName { get; set; }

        public string Password { get; set; }

        public DateTime? Birthday { get; set; }

    }
}
