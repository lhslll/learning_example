﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace MessageStore
{
    class Program
    {
        static void Main(string[] args)
        {
            var time = new System.Diagnostics.Stopwatch();

            var msgFile= new MessageFile(@"c:\1.msg", @"C:\1.pos");
            var data = "testtest";
            var bytes = Encoding.UTF8.GetBytes(data);
            bytes = new byte[4096];
            time.Start();
            for (var i = 1; i < 400000; i++)
            {
                if (msgFile.WriteMessage(bytes) == false) break;
            }
            for (var i = 0; i < msgFile.MessageCount; i++)
            {
                msgFile.ReadMessage(i);
            }
            time.Stop();
            Console.WriteLine(time.ElapsedMilliseconds);
            var bytes1= msgFile.ReadMessage(0);
            var data1 = Encoding.UTF8.GetString(bytes1);
            Console.WriteLine(data1);

        }
    }
}
