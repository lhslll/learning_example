﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    public class ManagerModel
    {
        /// <summary>
        /// 用户名
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 姓名
        /// </summary>
        public string RealName { get; set; }

        /// <summary>
        /// 所属组编码
        /// </summary>
        public string GroupCode { get; set; }

        /// <summary>
        /// 所属组
        /// </summary>
        public GroupModel Group { get; set; }
    }
}
