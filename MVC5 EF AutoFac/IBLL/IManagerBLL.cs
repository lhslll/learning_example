﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IBLL
{
    public interface IManagerBLL
    {
        Model.ManagerModel GetModel(string UserName);

        int Add(Model.ManagerModel model);

        int Update(Model.ManagerModel model);

        int Delete(string UserName);


    }
}
