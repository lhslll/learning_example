﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace EntityFrameWorkDAL
{
    public class ManagerDAL : Repository<Model.ManagerModel>,  IDAL.IManagerDAL
    {

        public ManagerDAL():base(new DBContext())
        {
            
        }

        public ManagerModel GetModel(string UserName)
        {
            var model = Search(r => r.UserName == UserName, "UserName asc").FirstOrDefault(); 
            return model;
        }

        public int Delete(string UserName)
        {
            return Delete(r => r.UserName == UserName);
        }
    }
}
