﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntityFrameWorkDAL.Map
{
    public class ManagerMap: EntityTypeConfiguration<Model.ManagerModel>
    {
        public ManagerMap()
        {
            this.ToTable("Manager");
            this.HasKey(e => e.UserName);
            this.Property(e => e.UserName).HasMaxLength(32).IsUnicode(true);
            this.Property(e => e.RealName).HasMaxLength(32).IsUnicode(true);
            this.Property(e => e.GroupCode).HasMaxLength(32).IsUnicode(true);
            this.HasRequired(e=>e.Group).WithMany().HasForeignKey(e=> e.GroupCode)
        }
    }
}
