﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DDD.Domain.Entitys
{
    public abstract class IEntity<T>
    {
        public T ID { get; protected set; }
    }
}
