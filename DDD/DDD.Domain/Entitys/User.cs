﻿using System;

namespace DDD.Domain.Entitys
{
    public class User:IAggregateRoot<int>
    {
        public string UserName { get; protected set; }

        public string Password { get; protected set; }

        public bool IsDisabled { get; protected set; }

        protected User()
        {

        }

        public User(int Id, string userName,string password,bool isDisabled)
        {
            this.ID = Id;
            this.UserName=userName;
            this.Password=password;
            this.IsDisabled=isDisabled;
        }


        public void ChangePassword(string oldPassword,string newPassword)
        {
            if (this.Password != oldPassword) throw new Exception("旧密码错误");
            this.Password = newPassword;
            
        }
        
        public void Disable()
        {
            this.IsDisabled = true;
        }

        public void Enable()
        {
            this.IsDisabled = false;
        }

    }
}
