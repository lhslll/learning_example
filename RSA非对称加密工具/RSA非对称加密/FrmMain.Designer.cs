﻿namespace RSA非对称加密
{
    partial class FrmMain
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.PanKeySize = new System.Windows.Forms.Panel();
            this.rad1024 = new System.Windows.Forms.RadioButton();
            this.rad2048 = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.LabCopyPublicKey = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.TbxPublicKey = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LabCopyPrivatekey = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TbxPrivateKey = new System.Windows.Forms.TextBox();
            this.BtnCreateKey = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.CobSignEncoding = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.PanRsa = new System.Windows.Forms.Panel();
            this.RabRsa = new System.Windows.Forms.RadioButton();
            this.RabRsa2 = new System.Windows.Forms.RadioButton();
            this.label6 = new System.Windows.Forms.Label();
            this.BtnCreateSign = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.TbxSignValue = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.TbxSignContent = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.TbxSignKey = new System.Windows.Forms.TextBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.CobVerifyEncoding = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.RabVerifyRsa = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.label10 = new System.Windows.Forms.Label();
            this.BtnVerify = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel7 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.TbxVerifySignValue = new System.Windows.Forms.TextBox();
            this.panel8 = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.TbxVerifyContent = new System.Windows.Forms.TextBox();
            this.panel9 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.TbxVerifyPublicKey = new System.Windows.Forms.TextBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.CobEncryptEncoding = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.BtnEncrypt = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.TbxEncryptValue = new System.Windows.Forms.TextBox();
            this.panel12 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.TbxEncryptCntent = new System.Windows.Forms.TextBox();
            this.panel13 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.TbxEncryptPublicKey = new System.Windows.Forms.TextBox();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.CobDecryptEncoding = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.BtnDecrypt = new System.Windows.Forms.Button();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.panel10 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.TbxDecryptContent = new System.Windows.Forms.TextBox();
            this.panel14 = new System.Windows.Forms.Panel();
            this.label20 = new System.Windows.Forms.Label();
            this.TbxDecryptValue = new System.Windows.Forms.TextBox();
            this.panel15 = new System.Windows.Forms.Panel();
            this.label21 = new System.Windows.Forms.Label();
            this.TbxDecryptPrivateKey = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.PanKeySize.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.PanRsa.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(772, 430);
            this.tabControl1.TabIndex = 8;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.PanKeySize);
            this.tabPage2.Controls.Add(this.tableLayoutPanel1);
            this.tabPage2.Controls.Add(this.BtnCreateKey);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(764, 404);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "生成密钥";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // PanKeySize
            // 
            this.PanKeySize.Controls.Add(this.rad1024);
            this.PanKeySize.Controls.Add(this.rad2048);
            this.PanKeySize.Location = new System.Drawing.Point(95, 21);
            this.PanKeySize.Name = "PanKeySize";
            this.PanKeySize.Size = new System.Drawing.Size(365, 20);
            this.PanKeySize.TabIndex = 17;
            // 
            // rad1024
            // 
            this.rad1024.AutoSize = true;
            this.rad1024.Location = new System.Drawing.Point(3, 2);
            this.rad1024.Name = "rad1024";
            this.rad1024.Size = new System.Drawing.Size(47, 16);
            this.rad1024.TabIndex = 11;
            this.rad1024.Text = "1024";
            this.rad1024.UseVisualStyleBackColor = true;
            // 
            // rad2048
            // 
            this.rad2048.AutoSize = true;
            this.rad2048.Checked = true;
            this.rad2048.Location = new System.Drawing.Point(69, 2);
            this.rad2048.Name = "rad2048";
            this.rad2048.Size = new System.Drawing.Size(47, 16);
            this.rad2048.TabIndex = 10;
            this.rad2048.TabStop = true;
            this.rad2048.Text = "2048";
            this.rad2048.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(19, 54);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(727, 333);
            this.tableLayoutPanel1.TabIndex = 16;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.LabCopyPublicKey);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.TbxPublicKey);
            this.panel2.Location = new System.Drawing.Point(3, 169);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(721, 161);
            this.panel2.TabIndex = 1;
            // 
            // LabCopyPublicKey
            // 
            this.LabCopyPublicKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabCopyPublicKey.AutoSize = true;
            this.LabCopyPublicKey.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LabCopyPublicKey.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.LabCopyPublicKey.Location = new System.Drawing.Point(689, 0);
            this.LabCopyPublicKey.Name = "LabCopyPublicKey";
            this.LabCopyPublicKey.Size = new System.Drawing.Size(29, 12);
            this.LabCopyPublicKey.TabIndex = 18;
            this.LabCopyPublicKey.Text = "复制";
            this.LabCopyPublicKey.Click += new System.EventHandler(this.LabCopyPublicKey_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 12);
            this.label3.TabIndex = 17;
            this.label3.Text = "公钥：";
            // 
            // TbxPublicKey
            // 
            this.TbxPublicKey.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxPublicKey.Location = new System.Drawing.Point(3, 27);
            this.TbxPublicKey.Multiline = true;
            this.TbxPublicKey.Name = "TbxPublicKey";
            this.TbxPublicKey.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxPublicKey.Size = new System.Drawing.Size(715, 132);
            this.TbxPublicKey.TabIndex = 16;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.LabCopyPrivatekey);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.TbxPrivateKey);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(721, 160);
            this.panel1.TabIndex = 0;
            // 
            // LabCopyPrivatekey
            // 
            this.LabCopyPrivatekey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabCopyPrivatekey.AutoSize = true;
            this.LabCopyPrivatekey.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LabCopyPrivatekey.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.LabCopyPrivatekey.Location = new System.Drawing.Point(689, 0);
            this.LabCopyPrivatekey.Name = "LabCopyPrivatekey";
            this.LabCopyPrivatekey.Size = new System.Drawing.Size(29, 12);
            this.LabCopyPrivatekey.TabIndex = 16;
            this.LabCopyPrivatekey.Text = "复制";
            this.LabCopyPrivatekey.Click += new System.EventHandler(this.LabCopyPrivatekey_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(41, 12);
            this.label2.TabIndex = 15;
            this.label2.Text = "私钥：";
            // 
            // TbxPrivateKey
            // 
            this.TbxPrivateKey.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxPrivateKey.Location = new System.Drawing.Point(3, 26);
            this.TbxPrivateKey.Multiline = true;
            this.TbxPrivateKey.Name = "TbxPrivateKey";
            this.TbxPrivateKey.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxPrivateKey.Size = new System.Drawing.Size(715, 131);
            this.TbxPrivateKey.TabIndex = 14;
            // 
            // BtnCreateKey
            // 
            this.BtnCreateKey.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnCreateKey.Location = new System.Drawing.Point(668, 18);
            this.BtnCreateKey.Name = "BtnCreateKey";
            this.BtnCreateKey.Size = new System.Drawing.Size(75, 23);
            this.BtnCreateKey.TabIndex = 11;
            this.BtnCreateKey.Text = "生成密钥";
            this.BtnCreateKey.UseVisualStyleBackColor = true;
            this.BtnCreateKey.Click += new System.EventHandler(this.BtnCreateKey_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 12);
            this.label1.TabIndex = 10;
            this.label1.Text = "密钥长度：";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.CobSignEncoding);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.PanRsa);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.BtnCreateSign);
            this.tabPage1.Controls.Add(this.tableLayoutPanel2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(764, 404);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "签名";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // CobSignEncoding
            // 
            this.CobSignEncoding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CobSignEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CobSignEncoding.FormattingEnabled = true;
            this.CobSignEncoding.Location = new System.Drawing.Point(496, 20);
            this.CobSignEncoding.Name = "CobSignEncoding";
            this.CobSignEncoding.Size = new System.Drawing.Size(150, 20);
            this.CobSignEncoding.TabIndex = 24;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(449, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 12);
            this.label8.TabIndex = 23;
            this.label8.Text = "编码：";
            // 
            // PanRsa
            // 
            this.PanRsa.Controls.Add(this.RabRsa);
            this.PanRsa.Controls.Add(this.RabRsa2);
            this.PanRsa.Location = new System.Drawing.Point(95, 19);
            this.PanRsa.Name = "PanRsa";
            this.PanRsa.Size = new System.Drawing.Size(136, 20);
            this.PanRsa.TabIndex = 22;
            // 
            // RabRsa
            // 
            this.RabRsa.AutoSize = true;
            this.RabRsa.Location = new System.Drawing.Point(3, 2);
            this.RabRsa.Name = "RabRsa";
            this.RabRsa.Size = new System.Drawing.Size(41, 16);
            this.RabRsa.TabIndex = 11;
            this.RabRsa.Text = "RSA";
            this.RabRsa.UseVisualStyleBackColor = true;
            // 
            // RabRsa2
            // 
            this.RabRsa2.AutoSize = true;
            this.RabRsa2.Checked = true;
            this.RabRsa2.Location = new System.Drawing.Point(69, 2);
            this.RabRsa2.Name = "RabRsa2";
            this.RabRsa2.Size = new System.Drawing.Size(47, 16);
            this.RabRsa2.TabIndex = 10;
            this.RabRsa2.TabStop = true;
            this.RabRsa2.Text = "RSA2";
            this.RabRsa2.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 23);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 12);
            this.label6.TabIndex = 21;
            this.label6.Text = "哈希算法：";
            // 
            // BtnCreateSign
            // 
            this.BtnCreateSign.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnCreateSign.Location = new System.Drawing.Point(668, 18);
            this.BtnCreateSign.Name = "BtnCreateSign";
            this.BtnCreateSign.Size = new System.Drawing.Size(75, 23);
            this.BtnCreateSign.TabIndex = 20;
            this.BtnCreateSign.Text = "生成签名";
            this.BtnCreateSign.UseVisualStyleBackColor = true;
            this.BtnCreateSign.Click += new System.EventHandler(this.BtnCreateSign_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.panel5, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(19, 54);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(727, 333);
            this.tableLayoutPanel2.TabIndex = 17;
            // 
            // panel5
            // 
            this.panel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel5.Controls.Add(this.label7);
            this.panel5.Controls.Add(this.TbxSignValue);
            this.panel5.Location = new System.Drawing.Point(3, 221);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(721, 109);
            this.panel5.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 12);
            this.label7.TabIndex = 17;
            this.label7.Text = "签名：";
            // 
            // TbxSignValue
            // 
            this.TbxSignValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxSignValue.Location = new System.Drawing.Point(3, 27);
            this.TbxSignValue.Multiline = true;
            this.TbxSignValue.Name = "TbxSignValue";
            this.TbxSignValue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxSignValue.Size = new System.Drawing.Size(715, 80);
            this.TbxSignValue.TabIndex = 16;
            // 
            // panel3
            // 
            this.panel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.TbxSignContent);
            this.panel3.Location = new System.Drawing.Point(3, 112);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(721, 103);
            this.panel3.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 12);
            this.label4.TabIndex = 17;
            this.label4.Text = "明文内容：";
            // 
            // TbxSignContent
            // 
            this.TbxSignContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxSignContent.Location = new System.Drawing.Point(3, 27);
            this.TbxSignContent.Multiline = true;
            this.TbxSignContent.Name = "TbxSignContent";
            this.TbxSignContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxSignContent.Size = new System.Drawing.Size(715, 74);
            this.TbxSignContent.TabIndex = 16;
            // 
            // panel4
            // 
            this.panel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel4.Controls.Add(this.label5);
            this.panel4.Controls.Add(this.TbxSignKey);
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(721, 103);
            this.panel4.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 12);
            this.label5.TabIndex = 15;
            this.label5.Text = "私钥：";
            // 
            // TbxSignKey
            // 
            this.TbxSignKey.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxSignKey.Location = new System.Drawing.Point(3, 26);
            this.TbxSignKey.Multiline = true;
            this.TbxSignKey.Name = "TbxSignKey";
            this.TbxSignKey.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxSignKey.Size = new System.Drawing.Size(715, 74);
            this.TbxSignKey.TabIndex = 14;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.CobVerifyEncoding);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.panel6);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.BtnVerify);
            this.tabPage3.Controls.Add(this.tableLayoutPanel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(764, 404);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "验签";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // CobVerifyEncoding
            // 
            this.CobVerifyEncoding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CobVerifyEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CobVerifyEncoding.FormattingEnabled = true;
            this.CobVerifyEncoding.Location = new System.Drawing.Point(496, 20);
            this.CobVerifyEncoding.Name = "CobVerifyEncoding";
            this.CobVerifyEncoding.Size = new System.Drawing.Size(150, 20);
            this.CobVerifyEncoding.TabIndex = 30;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(449, 23);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 12);
            this.label9.TabIndex = 29;
            this.label9.Text = "编码：";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.RabVerifyRsa);
            this.panel6.Controls.Add(this.radioButton2);
            this.panel6.Location = new System.Drawing.Point(95, 19);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(136, 20);
            this.panel6.TabIndex = 28;
            // 
            // RabVerifyRsa
            // 
            this.RabVerifyRsa.AutoSize = true;
            this.RabVerifyRsa.Location = new System.Drawing.Point(3, 2);
            this.RabVerifyRsa.Name = "RabVerifyRsa";
            this.RabVerifyRsa.Size = new System.Drawing.Size(41, 16);
            this.RabVerifyRsa.TabIndex = 11;
            this.RabVerifyRsa.Text = "RSA";
            this.RabVerifyRsa.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Checked = true;
            this.radioButton2.Location = new System.Drawing.Point(69, 2);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(47, 16);
            this.radioButton2.TabIndex = 10;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "RSA2";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(23, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 12);
            this.label10.TabIndex = 27;
            this.label10.Text = "哈希算法：";
            // 
            // BtnVerify
            // 
            this.BtnVerify.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnVerify.Location = new System.Drawing.Point(668, 18);
            this.BtnVerify.Name = "BtnVerify";
            this.BtnVerify.Size = new System.Drawing.Size(75, 23);
            this.BtnVerify.TabIndex = 26;
            this.BtnVerify.Text = "验证签名";
            this.BtnVerify.UseVisualStyleBackColor = true;
            this.BtnVerify.Click += new System.EventHandler(this.BtnVerify_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.panel7, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.panel8, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.panel9, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(19, 54);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(727, 333);
            this.tableLayoutPanel3.TabIndex = 25;
            // 
            // panel7
            // 
            this.panel7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel7.Controls.Add(this.label11);
            this.panel7.Controls.Add(this.TbxVerifySignValue);
            this.panel7.Location = new System.Drawing.Point(3, 221);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(721, 109);
            this.panel7.TabIndex = 2;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 12);
            this.label11.TabIndex = 17;
            this.label11.Text = "签名：";
            // 
            // TbxVerifySignValue
            // 
            this.TbxVerifySignValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxVerifySignValue.Location = new System.Drawing.Point(3, 27);
            this.TbxVerifySignValue.Multiline = true;
            this.TbxVerifySignValue.Name = "TbxVerifySignValue";
            this.TbxVerifySignValue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxVerifySignValue.Size = new System.Drawing.Size(715, 80);
            this.TbxVerifySignValue.TabIndex = 16;
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.Controls.Add(this.label12);
            this.panel8.Controls.Add(this.TbxVerifyContent);
            this.panel8.Location = new System.Drawing.Point(3, 112);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(721, 103);
            this.panel8.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 12);
            this.label12.TabIndex = 17;
            this.label12.Text = "明文内容：";
            // 
            // TbxVerifyContent
            // 
            this.TbxVerifyContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxVerifyContent.Location = new System.Drawing.Point(3, 27);
            this.TbxVerifyContent.Multiline = true;
            this.TbxVerifyContent.Name = "TbxVerifyContent";
            this.TbxVerifyContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxVerifyContent.Size = new System.Drawing.Size(715, 74);
            this.TbxVerifyContent.TabIndex = 16;
            // 
            // panel9
            // 
            this.panel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel9.Controls.Add(this.label13);
            this.panel9.Controls.Add(this.TbxVerifyPublicKey);
            this.panel9.Location = new System.Drawing.Point(3, 3);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(721, 103);
            this.panel9.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 12);
            this.label13.TabIndex = 15;
            this.label13.Text = "公钥：";
            // 
            // TbxVerifyPublicKey
            // 
            this.TbxVerifyPublicKey.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxVerifyPublicKey.Location = new System.Drawing.Point(3, 26);
            this.TbxVerifyPublicKey.Multiline = true;
            this.TbxVerifyPublicKey.Name = "TbxVerifyPublicKey";
            this.TbxVerifyPublicKey.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxVerifyPublicKey.Size = new System.Drawing.Size(715, 74);
            this.TbxVerifyPublicKey.TabIndex = 14;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.CobEncryptEncoding);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.BtnEncrypt);
            this.tabPage4.Controls.Add(this.tableLayoutPanel4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(764, 404);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "加密";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // CobEncryptEncoding
            // 
            this.CobEncryptEncoding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CobEncryptEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CobEncryptEncoding.FormattingEnabled = true;
            this.CobEncryptEncoding.Location = new System.Drawing.Point(496, 20);
            this.CobEncryptEncoding.Name = "CobEncryptEncoding";
            this.CobEncryptEncoding.Size = new System.Drawing.Size(150, 20);
            this.CobEncryptEncoding.TabIndex = 36;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(449, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 12);
            this.label14.TabIndex = 35;
            this.label14.Text = "编码：";
            // 
            // BtnEncrypt
            // 
            this.BtnEncrypt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnEncrypt.Location = new System.Drawing.Point(668, 18);
            this.BtnEncrypt.Name = "BtnEncrypt";
            this.BtnEncrypt.Size = new System.Drawing.Size(75, 23);
            this.BtnEncrypt.TabIndex = 32;
            this.BtnEncrypt.Text = "加密";
            this.BtnEncrypt.UseVisualStyleBackColor = true;
            this.BtnEncrypt.Click += new System.EventHandler(this.BtnEncrypt_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.panel11, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.panel12, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.panel13, 0, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(19, 54);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(727, 333);
            this.tableLayoutPanel4.TabIndex = 31;
            // 
            // panel11
            // 
            this.panel11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel11.Controls.Add(this.label16);
            this.panel11.Controls.Add(this.TbxEncryptValue);
            this.panel11.Location = new System.Drawing.Point(3, 221);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(721, 109);
            this.panel11.TabIndex = 2;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.ForeColor = System.Drawing.Color.Red;
            this.label16.Location = new System.Drawing.Point(3, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 12);
            this.label16.TabIndex = 17;
            this.label16.Text = "密文内容：";
            // 
            // TbxEncryptValue
            // 
            this.TbxEncryptValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxEncryptValue.Location = new System.Drawing.Point(3, 27);
            this.TbxEncryptValue.Multiline = true;
            this.TbxEncryptValue.Name = "TbxEncryptValue";
            this.TbxEncryptValue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxEncryptValue.Size = new System.Drawing.Size(715, 80);
            this.TbxEncryptValue.TabIndex = 16;
            // 
            // panel12
            // 
            this.panel12.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel12.Controls.Add(this.label17);
            this.panel12.Controls.Add(this.TbxEncryptCntent);
            this.panel12.Location = new System.Drawing.Point(3, 112);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(721, 103);
            this.panel12.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(3, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(65, 12);
            this.label17.TabIndex = 17;
            this.label17.Text = "明文内容：";
            // 
            // TbxEncryptCntent
            // 
            this.TbxEncryptCntent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxEncryptCntent.Location = new System.Drawing.Point(3, 27);
            this.TbxEncryptCntent.Multiline = true;
            this.TbxEncryptCntent.Name = "TbxEncryptCntent";
            this.TbxEncryptCntent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxEncryptCntent.Size = new System.Drawing.Size(715, 74);
            this.TbxEncryptCntent.TabIndex = 16;
            // 
            // panel13
            // 
            this.panel13.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel13.Controls.Add(this.label18);
            this.panel13.Controls.Add(this.TbxEncryptPublicKey);
            this.panel13.Location = new System.Drawing.Point(3, 3);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(721, 103);
            this.panel13.TabIndex = 0;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(3, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 12);
            this.label18.TabIndex = 15;
            this.label18.Text = "公钥：";
            // 
            // TbxEncryptPublicKey
            // 
            this.TbxEncryptPublicKey.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxEncryptPublicKey.Location = new System.Drawing.Point(3, 26);
            this.TbxEncryptPublicKey.Multiline = true;
            this.TbxEncryptPublicKey.Name = "TbxEncryptPublicKey";
            this.TbxEncryptPublicKey.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxEncryptPublicKey.Size = new System.Drawing.Size(715, 74);
            this.TbxEncryptPublicKey.TabIndex = 14;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.CobDecryptEncoding);
            this.tabPage5.Controls.Add(this.label15);
            this.tabPage5.Controls.Add(this.BtnDecrypt);
            this.tabPage5.Controls.Add(this.tableLayoutPanel5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(764, 404);
            this.tabPage5.TabIndex = 5;
            this.tabPage5.Text = "解密";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // CobDecryptEncoding
            // 
            this.CobDecryptEncoding.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CobDecryptEncoding.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CobDecryptEncoding.FormattingEnabled = true;
            this.CobDecryptEncoding.Location = new System.Drawing.Point(496, 20);
            this.CobDecryptEncoding.Name = "CobDecryptEncoding";
            this.CobDecryptEncoding.Size = new System.Drawing.Size(150, 20);
            this.CobDecryptEncoding.TabIndex = 40;
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(449, 23);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 12);
            this.label15.TabIndex = 39;
            this.label15.Text = "编码：";
            // 
            // BtnDecrypt
            // 
            this.BtnDecrypt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnDecrypt.Location = new System.Drawing.Point(668, 18);
            this.BtnDecrypt.Name = "BtnDecrypt";
            this.BtnDecrypt.Size = new System.Drawing.Size(75, 23);
            this.BtnDecrypt.TabIndex = 38;
            this.BtnDecrypt.Text = "解密";
            this.BtnDecrypt.UseVisualStyleBackColor = true;
            this.BtnDecrypt.Click += new System.EventHandler(this.BtnDecrypt_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.panel10, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.panel14, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.panel15, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(19, 54);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(727, 333);
            this.tableLayoutPanel5.TabIndex = 37;
            // 
            // panel10
            // 
            this.panel10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel10.Controls.Add(this.label19);
            this.panel10.Controls.Add(this.TbxDecryptContent);
            this.panel10.Location = new System.Drawing.Point(3, 221);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(721, 109);
            this.panel10.TabIndex = 2;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(3, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(65, 12);
            this.label19.TabIndex = 17;
            this.label19.Text = "明文内容：";
            // 
            // TbxDecryptContent
            // 
            this.TbxDecryptContent.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxDecryptContent.Location = new System.Drawing.Point(3, 27);
            this.TbxDecryptContent.Multiline = true;
            this.TbxDecryptContent.Name = "TbxDecryptContent";
            this.TbxDecryptContent.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxDecryptContent.Size = new System.Drawing.Size(715, 80);
            this.TbxDecryptContent.TabIndex = 16;
            // 
            // panel14
            // 
            this.panel14.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel14.Controls.Add(this.label20);
            this.panel14.Controls.Add(this.TbxDecryptValue);
            this.panel14.Location = new System.Drawing.Point(3, 112);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(721, 103);
            this.panel14.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(3, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(65, 12);
            this.label20.TabIndex = 17;
            this.label20.Text = "密文内容：";
            // 
            // TbxDecryptValue
            // 
            this.TbxDecryptValue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxDecryptValue.Location = new System.Drawing.Point(3, 27);
            this.TbxDecryptValue.Multiline = true;
            this.TbxDecryptValue.Name = "TbxDecryptValue";
            this.TbxDecryptValue.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxDecryptValue.Size = new System.Drawing.Size(715, 74);
            this.TbxDecryptValue.TabIndex = 16;
            // 
            // panel15
            // 
            this.panel15.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel15.Controls.Add(this.label21);
            this.panel15.Controls.Add(this.TbxDecryptPrivateKey);
            this.panel15.Location = new System.Drawing.Point(3, 3);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(721, 103);
            this.panel15.TabIndex = 0;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(41, 12);
            this.label21.TabIndex = 15;
            this.label21.Text = "私钥：";
            // 
            // TbxDecryptPrivateKey
            // 
            this.TbxDecryptPrivateKey.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TbxDecryptPrivateKey.Location = new System.Drawing.Point(3, 26);
            this.TbxDecryptPrivateKey.Multiline = true;
            this.TbxDecryptPrivateKey.Name = "TbxDecryptPrivateKey";
            this.TbxDecryptPrivateKey.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TbxDecryptPrivateKey.Size = new System.Drawing.Size(715, 74);
            this.TbxDecryptPrivateKey.TabIndex = 14;
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 454);
            this.Controls.Add(this.tabControl1);
            this.Name = "FrmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RSA非对称加密";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.PanKeySize.ResumeLayout(false);
            this.PanKeySize.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.PanRsa.ResumeLayout(false);
            this.PanRsa.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel8.ResumeLayout(false);
            this.panel8.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.panel12.ResumeLayout(false);
            this.panel12.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel10.PerformLayout();
            this.panel14.ResumeLayout(false);
            this.panel14.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TbxPublicKey;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TbxPrivateKey;
        private System.Windows.Forms.Button BtnCreateKey;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Panel PanKeySize;
        private System.Windows.Forms.RadioButton rad1024;
        private System.Windows.Forms.RadioButton rad2048;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TbxSignContent;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox TbxSignKey;
        private System.Windows.Forms.Button BtnCreateSign;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TbxSignValue;
        private System.Windows.Forms.Panel PanRsa;
        private System.Windows.Forms.RadioButton RabRsa;
        private System.Windows.Forms.RadioButton RabRsa2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox CobSignEncoding;
        private System.Windows.Forms.ComboBox CobVerifyEncoding;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.RadioButton RabVerifyRsa;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button BtnVerify;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox TbxVerifySignValue;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox TbxVerifyContent;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox TbxVerifyPublicKey;
        private System.Windows.Forms.ComboBox CobEncryptEncoding;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button BtnEncrypt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox TbxEncryptValue;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox TbxEncryptCntent;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox TbxEncryptPublicKey;
        private System.Windows.Forms.ComboBox CobDecryptEncoding;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button BtnDecrypt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox TbxDecryptContent;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox TbxDecryptValue;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox TbxDecryptPrivateKey;
        private System.Windows.Forms.Label LabCopyPrivatekey;
        private System.Windows.Forms.Label LabCopyPublicKey;
    }
}

